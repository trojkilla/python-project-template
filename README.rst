***********************
Python Project Template
***********************

.. image:: https://travis-ci.org/fhightower-templates/python-project-template.svg?branch=master
    :target: https://travis-ci.org/fhightower-templates/python-project-template

This is a template for Python projects based on the wonderful template here: `https://github.com/audreyr/cookiecutter-pypackage/ <https://github.com/audreyr/cookiecutter-pypackage/>`_.

Usage
=====

``cookiecutter https://gitlab.com/fhightower-templates/python-project-template.git`` 
